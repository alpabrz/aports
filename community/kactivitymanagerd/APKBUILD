# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivitymanagerd
pkgver=5.21.2
pkgrel=0
pkgdesc="System service to manage user's activities and track the usage patterns"
# armhf blocked by qt5-qtdeclarative
# s390x, mips64 blocked by kxmlgui, kio
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-only OR GPL-3.0-only"
depends="qt5-qtbase-sqlite"
makedepends="
	boost-dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kio-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kactivitymanagerd-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="f988899df27f4fcdca61367599889226712d13a09c67c6dd0812c2dfbcca9e8502af6352625776e49af6ed761e08ecc8318f7bd4e3a4ccabe082c4f3dd3a5df2  kactivitymanagerd-5.21.2.tar.xz"
