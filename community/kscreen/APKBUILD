# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kscreen
pkgver=5.21.2
pkgrel=0
pkgdesc="KDE's screen management software"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by kconfigwidgets
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="hicolor-icon-theme"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkscreen-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsensors-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kscreen-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kscreen-kded-configtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kscreen-kded-configtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="c313b3404b58f97b5f2b005261a74171de5a37c769ec9348020108357dad0d04c96f4e42e041c5c8189ba3f404768ff995c24ae94e063b0f4edeeb88b8f4cc36  kscreen-5.21.2.tar.xz"
